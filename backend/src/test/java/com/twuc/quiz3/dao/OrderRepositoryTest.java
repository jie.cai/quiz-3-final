package com.twuc.quiz3.dao;

import com.twuc.quiz3.entity.Goods;
import com.twuc.quiz3.entity.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OrderRepositoryTest {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private EntityManager entityManager;

    @Test
    void should_add_order_for_a_goods() {
        Goods goods = goodsRepository.save(new Goods("可乐", 2.5, "瓶", "/kele.png"));
        entityManager.flush();

        Order order = new Order(100, goods);
        order.setGoods(goods);
        Order expectedOrder = orderRepository.save(order);
        entityManager.flush();

        Order savedOrder = orderRepository.findById(expectedOrder.getId()).orElseThrow(NoSuchElementException::new);
        assertEquals(Integer.valueOf(100), savedOrder.getCount());
        assertEquals("可乐", savedOrder.getGoods().getName());
    }
}
