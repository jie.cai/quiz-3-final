package com.twuc.quiz3.dao;

import com.twuc.quiz3.entity.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class GoodsRepositoryTest {

    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void should_save_a_goods() {
        Goods expectedGoods = goodsRepository.save(new Goods("可乐", 2.34, "瓶", "/kele.png"));
        entityManager.flush();
        Goods goods = goodsRepository.findById(expectedGoods.getId()).orElseThrow(NoSuchElementException::new);
        assertEquals("可乐", goods.getName());
    }
}
