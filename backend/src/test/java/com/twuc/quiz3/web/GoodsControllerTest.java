package com.twuc.quiz3.web;

import com.twuc.quiz3.entity.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class GoodsControllerTest extends  ApiTestBase {
    @Test
    void should_add_a_goods() throws Exception {
        mockMvc.perform(
                post("/api/goods")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(serialize(new Goods("可乐", 2.5, "瓶", "/kele.png")))
        )
                .andExpect(status().isCreated())
                .andExpect(header().exists("location"));
    }

    @Test
    void should_get_goods_list() throws Exception {
        mockMvc.perform(
                get("/api/goods")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }
}
