package com.twuc.quiz3.web;

import com.twuc.quiz3.dao.GoodsRepository;
import com.twuc.quiz3.dao.OrderRepository;
import com.twuc.quiz3.entity.Goods;
import com.twuc.quiz3.entity.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderControllerTest extends ApiTestBase {
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private EntityManager entityManager;

    @Test
    void should_add_order_for_a_goods() throws Exception {
        Goods goods = goodsRepository.save(new Goods("可乐", 2.5, "瓶", "/kele.png"));
        entityManager.flush();

        mockMvc.perform(
                post("/api/orders")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\n" +
                            "  \"goodsId\": "+goods.getId()+"\n" +
                            "}")
        )
                .andExpect(status().isOk());

        Order order = orderRepository.findByGoodsId(goods.getId());
        assertEquals("可乐", order.getGoods().getName());
    }

    @Test
    void should_get_order_list() throws Exception {
        Goods goods = goodsRepository.save(new Goods("可乐", 2.5, "瓶", "/kele.png"));
        entityManager.flush();

        orderRepository.save(new Order(1, goods));
        orderRepository.save(new Order(2, goods));
        orderRepository.save(new Order(3, goods));

        mockMvc.perform(
                get("/api/orders")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }
}
