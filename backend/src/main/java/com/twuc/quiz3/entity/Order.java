package com.twuc.quiz3.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Integer count;
    @ManyToOne(optional = true)
    @JoinColumn(name = "goods_id")
    private Goods goods;

    public Order() {
    }

    public Order(Integer count, Goods goods) {
        this.count = count;
        this.goods = goods;
    }

    public Long getId() {
        return id;
    }

    public Integer getCount() {
        return count;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public void countAdd() {
        this.count = this.count + 1;
    }
}
