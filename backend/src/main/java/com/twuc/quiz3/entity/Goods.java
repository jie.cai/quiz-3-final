package com.twuc.quiz3.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    @Column
    private String name;
    @NotNull
    @Column
    private Double price;
    @NotBlank
    @Column
    private String unit;
    @NotBlank
    @Column
    private String preview;

    public Goods() {
    }

    public Goods(String name, Double price, String unit, String preview) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.preview = preview;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPreview() {
        return preview;
    }
}
