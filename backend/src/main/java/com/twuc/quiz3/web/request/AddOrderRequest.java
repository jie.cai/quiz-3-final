package com.twuc.quiz3.web.request;

public class AddOrderRequest {
    private Long goodsId;

    public AddOrderRequest() {
    }

    public AddOrderRequest(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() {
        return goodsId;
    }
}
