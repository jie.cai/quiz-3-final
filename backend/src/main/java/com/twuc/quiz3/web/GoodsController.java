package com.twuc.quiz3.web;

import com.twuc.quiz3.dao.GoodsRepository;
import com.twuc.quiz3.entity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/goods")
public class GoodsController {

    private final GoodsRepository goodsRepository;

    public GoodsController(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @PostMapping
    public ResponseEntity addGoods(
            @Valid @RequestBody Goods goods
    ) throws URISyntaxException {
        Goods presentGoods = goodsRepository.save(goods);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .location(new URI("/api/goods/" + presentGoods.getId()))
                .build();
    }

    @GetMapping
    public List<Goods> goodsList() {
        return goodsRepository.findAll();
    }
}
