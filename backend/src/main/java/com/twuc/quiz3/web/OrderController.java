package com.twuc.quiz3.web;

import com.twuc.quiz3.dao.GoodsRepository;
import com.twuc.quiz3.dao.OrderRepository;
import com.twuc.quiz3.entity.Order;
import com.twuc.quiz3.web.request.AddOrderRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    private final OrderRepository orderRepository;
    private final GoodsRepository goodsRepository;


    public OrderController(OrderRepository orderRepository, GoodsRepository goodsRepository) {
        this.orderRepository = orderRepository;
        this.goodsRepository = goodsRepository;
    }

    @PostMapping
    public void addOrder(
            @Valid @RequestBody AddOrderRequest request
    ) {
        goodsRepository.findById(request.getGoodsId()).ifPresent(goods -> {
            Order order = orderRepository.findByGoodsId(request.getGoodsId());
            order = order == null ? new Order(0, goods) : order;
            order.countAdd();
            orderRepository.save(order);
        });
    }

    @GetMapping
    public List<Order> orderList() {
        return orderRepository.findAll();
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(
            @PathVariable Long id
    ) {
        orderRepository.deleteById(id);
    }
}
