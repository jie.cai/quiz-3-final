package com.twuc.quiz3.dao;

import com.twuc.quiz3.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsRepository extends JpaRepository<Goods, Long> {
}
