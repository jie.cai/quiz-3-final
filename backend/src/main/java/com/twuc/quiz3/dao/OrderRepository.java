package com.twuc.quiz3.dao;

import com.twuc.quiz3.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findByGoodsId(Long goodsId);
}
