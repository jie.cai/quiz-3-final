create table goods (
    id bigint auto_increment primary key,
    name text not null,
    price decimal(13, 4) not null,
    unit text not null,
    preview text not null
);
