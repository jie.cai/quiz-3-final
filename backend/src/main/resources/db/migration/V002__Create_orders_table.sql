create table orders (
    id bigint AUTO_INCREMENT PRIMARY KEY,
    goods_id bigint not null,
    count int not null,
    foreign key (goods_id) references goods(id)
);
