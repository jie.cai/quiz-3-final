import React from 'react';
import ReactDOM from 'react-dom';
import 'modern-normalize'
import './styles/index.less';

import { App } from './App';

ReactDOM.render(<App />, document.getElementById('app'));
