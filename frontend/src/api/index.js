export const addGoods = ({ name, price, unit, preview }) =>
  fetch('/api/goods', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify({ name, price, unit, preview })
  })

export const goodsList = () =>
  fetch('/api/goods')
    .then(response => response.json())

export const addOrder = goodsId =>
  fetch('/api/orders', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify({ goodsId })
  })

export const orderList = () =>
  fetch('/api/orders')
    .then(response => response.json())

export const deleteOrder = orderId => 
  fetch(`/api/orders/${orderId}`, {
    method: 'DELETE'
  })
