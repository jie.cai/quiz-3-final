import React, { useEffect, useState } from 'react';
import { goodsList, addOrder } from '../api';

export const Goods = () => {
  const [list, setList] = useState([]);

  useEffect(() => {
    goodsList()
      .then(setList)
  }, []);

  const handleAdd = (goods) => {
    addOrder(goods.id)
  }

  return (
    <div className="page goods">
      <ul>
        {
          list.map(goods => {
            const {id, name, price, unit} = goods;
            return (
              <li key={id}>
                <img src={require('../assets/preview/kele.png')} />
                <div className="shop-info">
                  <div className="name">{name}</div>
                  <div className="price">单价：{price}元/{unit}</div>
                  <button onClick={() => handleAdd(goods)}>+</button>
                </div>
              </li>
            )
          })
        }
      </ul>
    </div>
  );
};
