import React, { useState } from 'react';
import { addGoods } from '../api'

export const GoodsAdd = props => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState();
  const [unit, setUnit] = useState('');
  const [preview, setPreview] = useState('');

  const valid = name && price && unit && preview;
  const values = { name, price, unit, preview };

  const handleSubmit = event => {
    event.preventDefault();
    addGoods(values)
      .then(() => {
        props.history.push('/');
      })
      .catch(error => {
        console.error(error);
        alert('添加失败');
      });
  }

  return (
    <div className="page goods-add">
      <h2>添加商品</h2>
      <form onSubmit={handleSubmit}>
        <label className="required">名称</label>
        <input type="text" name="name" placeholder="名称" onChange={event => setName(event.target.value)} />
        <br />
        <label className="required">价格</label>
        <input type="text" name="price" placeholder="价格" onChange={event => parseFloat(setPrice(event.target.value))} />
        <br />
        <label className="required">单位</label>
        <input type="text" name="unit" placeholder="单位" onChange={event => setUnit(event.target.value)} />
        <br />
        <label className="required">图片</label>
        <input type="text" name="preview" placeholder="图片" onChange={event => parseFloat(setPreview(event.target.value))} />
        <br />
        <input disabled={!valid} type="submit" value="提交" />
      </form>
    </div>
  );
};
