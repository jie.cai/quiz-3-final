import React, { useEffect, useState } from 'react';
import { orderList, deleteOrder } from '../api';

export const Orders = () => {
  const [list, setList] = useState([]);

  useEffect(() => {
    orderList()
      .then(setList)
  }, []);

  const handleDelete = order => {
    deleteOrder(order.id)
      .then(() => {
        orderList()
          .then(setList)
      })
  }

  return (
    <div className="page orders">
      <ul>
        {
          list.map(order => {
            const { id, count, goods: { name } } = order;
            return (
              <li key={id}>{ name } { count }<button onClick={() => handleDelete(order)}>Delete</button></li>
            );
          })
        }
      </ul>
    </div>
  );
};
