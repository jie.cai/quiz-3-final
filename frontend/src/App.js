import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import { Goods } from './pages/Goods';
import { Orders } from './pages/Orders';
import { GoodsAdd } from './pages/GoodsAdd';

export const App = () => {

  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <NavLink exact activeClassName='current' to="/">商城</NavLink>
            </li>
            <li>
              <NavLink activeClassName='current' to="/orders">订单</NavLink>
            </li>
            <li>
              <NavLink activeClassName='current' to="/goods/add">添加商品</NavLink>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route exact path="/" component={Goods} />
          <Route path="/orders" component={Orders} />
          <Route path="/goods/add" component={GoodsAdd} />
        </Switch>
      </div>
    </Router>
  );
};
